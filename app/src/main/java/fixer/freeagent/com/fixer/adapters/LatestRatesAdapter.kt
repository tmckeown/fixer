package fixer.freeagent.com.fixer.adapters

import android.content.Context
import android.provider.Settings.Global.getString
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import fixer.freeagent.com.fixer.R
import fixer.freeagent.com.fixer.model.Rates
import kotlinx.android.synthetic.main.cell_latest_rates.view.*

class LatestRatesAdapter(val items: ArrayList<Rates>, val context: Context) : RecyclerView.Adapter<ViewHolderLatestRates>() {

    /**
     * OnCreate setup view inflater and attach layout
     */
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolderLatestRates {
        return ViewHolderLatestRates(LayoutInflater.from(context).inflate(R.layout.cell_latest_rates, parent, false))
    }


    /**
     * get the number of reates
     */
    override fun getItemCount(): Int {
        return items.size
    }


    /**
     * Render data into the cell for a row number
     */
    override fun onBindViewHolder(holder: ViewHolderLatestRates, position: Int) {
        holder?.tvCountryCode?.text = items.get(position).currencyName
        holder?.tvRate?.text = "%.2f".format(items.get(position).displayValue)
        holder?.tvRateCountry.text = items.get(position).currencyName
        Picasso.get().load(getImageUrl(items.get(position).currencyName)).into(holder?.ivCountryFlag)
    }


    /**
     * Get image URL by substituting the image name in the url
     */
    fun getImageUrl(countryCode: String): String {
        return String.format(context.getString(R.string.str_flags_url, countryCode.substring(0, 2)))
    }
}


/**
 * Setup view holder
 */
class ViewHolderLatestRates(view: View) : RecyclerView.ViewHolder(view) {
    val ivCountryFlag = view.ivCountryFlag
    val tvCountryCode = view.tvCountryCode
    val tvRate = view.tvRate
    val tvRateCountry = view.tvRateCountry
}
