package fixer.freeagent.com.fixer.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import fixer.freeagent.com.fixer.R
import fixer.freeagent.com.fixer.model.ExchangeRates
import fixer.freeagent.com.fixer.utils.UtilsDate
import kotlinx.android.synthetic.main.cell_historical_rates.view.*

class HistoricalRatesAdapter(val items: ArrayList<ExchangeRates>, val context: Context) : RecyclerView.Adapter<ViewHolder>() {

    /**
     * OnCreate setup view inflater and attach layout
     */
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.cell_historical_rates, parent, false))
    }


    /**
     * get the number of historical exchange ratesContainer
     */
    override fun getItemCount(): Int {
        return items.size
    }


    /**
     * Render data into the cell for a row number
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder?.tvCurrencyDate?.text = UtilsDate.toSimpleString(items.get(position).date)
        holder?.tvFirstCurrency?.text = "%.2f".format(items.get(position).rates.ratesContainer.get(0).displayValue)
        holder?.tvFirstCurrencyCode?.text = items.get(position).rates.ratesContainer.get(0).currencyName
        holder?.tvSecondCurrency?.text = "%.2f".format(items.get(position).rates.ratesContainer.get(1).displayValue)
        holder?.tvSecondCurrencyCode?.text = items.get(position).rates.ratesContainer.get(1).currencyName
    }
}


/**
 * Setup view holder
 */
class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val tvCurrencyDate = view.tvCurrencyDate
    val tvFirstCurrency = view.tvFirstCurrency
    val tvFirstCurrencyCode = view.tvFirstCurrencyCode
    val tvSecondCurrency = view.tvSecondCurrency
    val tvSecondCurrencyCode = view.tvSecondCurrencyCode
}