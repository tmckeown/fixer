package fixer.freeagent.com.fixer.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import fixer.freeagent.com.fixer.model.Countries
import fixer.freeagent.com.fixer.repository.CountryListRepository

class CountryListViewModel(application: Application) : AndroidViewModel(application) {

    private val countryListRepository = CountryListRepository()
    val mutableListCountryList = MutableLiveData<ArrayList<Countries>>()


    /**
     * Called when view model is instantiated
     */
    init {
        callStringResourcesForCountryList(application)
    }


    /**
     * Fetch country list from string resources
     */
    private fun callStringResourcesForCountryList(context : Context) {
        mutableListCountryList.postValue(countryListRepository.fetchCountryList(context))
    }


}