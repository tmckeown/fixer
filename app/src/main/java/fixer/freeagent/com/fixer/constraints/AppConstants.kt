package fixer.freeagent.com.fixer.constraints

import android.content.Context

object AppConstants {

    // web service
    const val CONST_API_BASE_URL = "http://data.fixer.io/api/"
    const val CONST_API_PATH_LATEST = "latest?"
    const val CONST_API_PATH_HISTORICAL = "{date}?"
    const val CONST_API_ACCESS_KEY = "c75d2863bda928ae954a455c3c5fee06"
    const val CONST_API_COUNTRIES = "USD,GBP,JPY,AUD,CAD,CHF,CNY,SEK,NZD"
    const val CONST_API_PARAM_KEY = "access_key"
    const val CONST_API_PARAM_SYMBOLS = "symbols"
    const val CONST_API_PARAM_DATE = "date"
    const val CONST_API_PARAM_BASE = "base"


    // intent extras
    const val CONST_INTENT_AMOUNT = "CONST_INTENT_AMOUNT"

    // shared prefs
    const val CONST_PREF_NAME = "Fixer"
    const val CONST_PREF_MODE_PRIVATE = Context.MODE_PRIVATE
    const val CONST_PREF_COUNTRY_LIST = "country_list"

    // validation
    const val CONST_MIN_AMOUNT = 1
    const val CONST_MAX_AMOUNT = 1000

}

