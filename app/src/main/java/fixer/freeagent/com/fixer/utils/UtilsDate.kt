package fixer.freeagent.com.fixer.utils

import android.os.Build
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

object UtilsDate {

    /**
     * return last 5 calendar days for multiple android build versions
     */
    fun getHistoricalDateRange(): Array<String> {
        var dateRange: Array<String>
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
            dateRange = arrayOf(
                LocalDateTime.now().format(formatter),
                LocalDateTime.now().minusDays(1).format(formatter),
                LocalDateTime.now().minusDays(2).format(formatter),
                LocalDateTime.now().minusDays(3).format(formatter),
                LocalDateTime.now().minusDays(4).format(formatter)
            )
        } else {
            var date = Date();
            val dateFormatter = SimpleDateFormat("yyyy-MM-dd")
            dateRange = arrayOf(
                dateFormatter.format(date),
                dateFormatter.format(date.getTime()-(24*60*60*1000)),
                dateFormatter.format(date.getTime()-(48*60*60*1000)),
                dateFormatter.format(date.getTime()-(72*60*60*1000)),
                dateFormatter.format(date.getTime()-(96*60*60*1000))
            )
        }
        return dateRange
    }


    /**
     * Simple date formatter
     */
    fun toSimpleString(date: Date) : String {
        val format = SimpleDateFormat("DD MMM yyy")
        return format.format(date)
    }

}