package fixer.freeagent.com.fixer.activities

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import fixer.freeagent.com.fixer.R
import fixer.freeagent.com.fixer.adapters.CountryListAdapter
import fixer.freeagent.com.fixer.model.Countries
import fixer.freeagent.com.fixer.viewmodel.CountryListViewModel
import kotlinx.android.synthetic.main.activity_country_list.*
import kotlinx.android.synthetic.main.include_toolbar.*

class ActivityCountryList : AppCompatActivity() {

    // init view model
    private val countryListViewModel: CountryListViewModel
        get() = ViewModelProviders.of(this).get(CountryListViewModel::class.java)


    /**
     * Lifecycle OnCreate
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_country_list)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setupObservables()
    }


    /**
     * Handle toolbar back button action
     */
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }


    /**
     * Setup observables on view models
     */
    private fun setupObservables() {
        countryListViewModel.mutableListCountryList.observe(this, Observer {
            setupRecyclerView(it!!)
        })
    }


    /**
     * Setup the recycler view with list of counties
     */
    private fun setupRecyclerView(countryList : ArrayList<Countries>) {
        recyclerListCountries.layoutManager = LinearLayoutManager(this)
        recyclerListCountries.adapter = CountryListAdapter(countryList, this)
    }

}
