package fixer.freeagent.com.fixer.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import fixer.freeagent.com.fixer.model.ExchangeRates
import fixer.freeagent.com.fixer.repository.HistoricalRatesRepository
import fixer.freeagent.com.fixer.utils.UtilsDate
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*

class HistoricalRatesViewModel : ViewModel() {

    private val historicalRatesRepository = HistoricalRatesRepository()
    val mutableListLatestRates = MutableLiveData<ArrayList<ExchangeRates>>()
    var amount: Double = 1.0


    /**
     * API Call to get historical ratesContainer
     */
    fun callApiForHistoricalRates(firstCurrency: String, secondCurrency: String, amount: Double) {
        historicalRatesRepository.requestHistoricalRates(getRequestDateRange(), firstCurrency, secondCurrency)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(this::handleManyHistoricalRatesResponse, this::handleManyHistoricalRatesError)
    }


    /**
     * API Response for get latest ratesContainer
     */
    private fun handleManyHistoricalRatesResponse(exchangeRatesRemote: ArrayList<ExchangeRates>) {
        mutableListLatestRates.postValue(raiseRatesByAmount(exchangeRatesRemote))
    }


    /**
     * API Error response for get latest ratesContainer should use this to report error or notify user
     */
    private fun handleManyHistoricalRatesError(error: Throwable) {
        print(error.localizedMessage)
    }


    /**
     * Update the rate values with the user specified amount
     */
    fun raiseRatesByAmount(exchangeRates: ArrayList<ExchangeRates>) : ArrayList<ExchangeRates> {
        exchangeRates.forEach {
            it.rates.ratesContainer[0].displayValue = it.rates.ratesContainer[0].displayValue * amount;
            it.rates.ratesContainer[1].displayValue = it.rates.ratesContainer[1].displayValue * amount;
        }
        return exchangeRates
    }


    /**
     * Get a list of dates for the last five days
     */
    private fun getRequestDateRange(): Array<String> {
        return UtilsDate.getHistoricalDateRange()
    }

}