package fixer.freeagent.com.fixer.interfaces

import fixer.freeagent.com.fixer.constraints.AppConstants
import fixer.freeagent.com.fixer.model.ExchangeRates
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface FixerAPIService {

    /**
     * Fixer API /Latest - Get a list of the latest exchange ratesContainer for currencies
     */
    @GET(AppConstants.CONST_API_PATH_LATEST)
    fun getLatestExchangeRatesForCurrencies(
        @Query(AppConstants.CONST_API_PARAM_KEY) accessKey: String,
        @Query(AppConstants.CONST_API_PARAM_SYMBOLS) currencies: String
    ): Observable<ExchangeRates>


    /**
     * Fixer API /Historical - GET a list of historical exchange ratesContainer for currencies
     */
    @GET(AppConstants.CONST_API_PATH_HISTORICAL)
    fun getHistoricalRatesForCurrencies(
        @Path(AppConstants.CONST_API_PARAM_DATE) date: String,
        @Query(AppConstants.CONST_API_PARAM_KEY) accessKey: String,
        @Query(AppConstants.CONST_API_PARAM_BASE) baseCurrency: String,
        @Query(AppConstants.CONST_API_PARAM_SYMBOLS) currencies: String
    ): Observable<ExchangeRates>



}