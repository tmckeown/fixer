package fixer.freeagent.com.fixer.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import fixer.freeagent.com.fixer.R
import fixer.freeagent.com.fixer.model.Countries
import fixer.freeagent.com.fixer.preferences.AppPreferences
import kotlinx.android.synthetic.main.cell_country_picker.view.*

class CountryListAdapter(val countryArrayList: ArrayList<Countries>, val context: Context) : RecyclerView.Adapter<ViewHolderCountries>() {

    /**
     * OnCreate setup view inflater and attach layout
     */
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolderCountries {
        return ViewHolderCountries(LayoutInflater.from(context).inflate(R.layout.cell_country_picker, parent, false))
    }


    /**
     * Render data into the cell for a row number
     */
    override fun onBindViewHolder(holder: ViewHolderCountries, position: Int) {
        holder?.countryName?.text = countryArrayList.get(position).name
        holder?.checkBox!!.isChecked = countryArrayList[position].isSelected
        Picasso.get().load(getImageUrl(countryArrayList.get(position).name.substring(0, 2))).into(holder?.countryFlag)
        holder.checkBox!!.setOnClickListener {
            if (countryArrayList[position].isSelected) {
                countryArrayList[position].isSelected = false
                AppPreferences.removeCountry(countryArrayList.get(position).name.substring(0, 3))
            } else {
                countryArrayList[position].isSelected = true
                AppPreferences.addCountry(countryArrayList.get(position).name.substring(0, 3))
            }
        }
    }


    /**
     * get the number of countries to display
     */
    override fun getItemCount(): Int {
        return countryArrayList.size
    }


    /**
     * Get image URL by substituting the image name in the url
     */
    fun getImageUrl(countryCode: String): String {
        return context.getString(R.string.str_flags_url, countryCode.substring(0,2))
    }
}


/**
 * Setup view holder
 */
class ViewHolderCountries(view: View) : RecyclerView.ViewHolder(view) {
    val checkBox = view.cbCountrySelected
    val countryName = view.tvCountry
    val countryFlag = view.ivCountrySelectionFlag
}

