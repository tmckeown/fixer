package fixer.freeagent.com.fixer.activities

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.inputmethod.EditorInfo
import fixer.freeagent.com.fixer.R
import fixer.freeagent.com.fixer.adapters.LatestRatesAdapter
import fixer.freeagent.com.fixer.constraints.AppConstants
import fixer.freeagent.com.fixer.model.Rates
import fixer.freeagent.com.fixer.viewmodel.LatestRatesViewModel
import kotlinx.android.synthetic.main.activity_latest_rates.*
import kotlinx.android.synthetic.main.include_toolbar.*
import android.view.inputmethod.InputMethodManager


class ActivityLatestRates : AppCompatActivity() {

    // init view model
    private val latestRatesViewModel: LatestRatesViewModel
        get() = ViewModelProviders.of(this).get(LatestRatesViewModel::class.java)


    /**
     * Lifecycle method onCreate
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_latest_rates)
        setSupportActionBar(toolbar)
        setupObservables()
        attachUIEventListeners()
    }


    /**
     * Lifecycle on resume reset amount and call web service to get rates
     */
    override fun onResume() {
        super.onResume()
        etAmount.setText("1")
        latestRatesViewModel.callApiForLatestRates()
    }


    /**
     * Inflate the toolbar menu
     */
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_latest_rates, menu)
        return true
    }


    /**
     * Toolbar menu click listener
     */
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.menuActionHistorical -> {
                launchHistoricalPrices()
                return true;
            }
            R.id.menuActionCountries -> {
                launchSettings()
                return true;
            }
        }
        return super.onOptionsItemSelected(item)
    }


    /**
     * Launch settings activity
     */
    private fun launchSettings() {
        val intent = Intent(this, ActivityCountryList::class.java)
        startActivity(intent);
    }


    /**
     * Launch historical prices activity
     */
    private fun launchHistoricalPrices() {
        val intent = Intent(this, ActivityHistoricalRates::class.java)
        intent.putExtra(AppConstants.CONST_INTENT_AMOUNT, etAmount.text.toString().toDouble())
        startActivity(intent)
    }


    /**
     * Setup observables on view models
     */
    private fun setupObservables() {
        latestRatesViewModel.mutableListLatestRates.observe(this, Observer {
            setupListView(it!!)
        })
    }


    /**
     * Listen for UI events and click listeners
     */
    private fun attachUIEventListeners() {
        // click listener for user amount input
        llTopContainer.setOnClickListener {
            etAmount.text.clear()
            etAmount.requestFocus();
            etAmount.setFocusableInTouchMode(true);
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(etAmount, InputMethodManager.SHOW_FORCED)
        }

        // click listener for edit text amount
        etAmount.setOnClickListener{
            etAmount.text.clear()
        }

        // soft keyboard ime button done button listener
        etAmount.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                if(validateUserInput()) {
                    latestRatesViewModel.recalculateLatestRates(etAmount.text.toString().toDouble())
                } else {
                    showValidationDialog()
                }
            }
            return@setOnEditorActionListener false
        }
    }


    /**
     * Show dialog to alert user about invalid amount
     */
    fun showValidationDialog() {
        val builder = AlertDialog.Builder(this@ActivityLatestRates)
        builder.setTitle(resources.getString(R.string.str_dialog_validation_title))
        builder.setMessage(resources.getString(R.string.str_dialog_validation_message))
        builder.setPositiveButton(resources.getString(R.string.str_dialog_validation_okay)) {
            dialog, which ->
            dialog.dismiss()
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }


    /**
     * Validate user input amount not to be empty and between 1 & 1000
     */
    fun validateUserInput(): Boolean {
        return(etAmount.text.isNotEmpty() && (etAmount.text.toString().toDouble() >= AppConstants.CONST_MIN_AMOUNT)
                && (etAmount.text.toString().toDouble() <= AppConstants.CONST_MAX_AMOUNT))
    }


    /**
     * Setup recycler list view
     */
    private fun setupListView(latestRates: ArrayList<Rates>) {
        lvLatestRates.layoutManager = LinearLayoutManager(this)
        lvLatestRates.adapter = LatestRatesAdapter(latestRates, this)
    }

}
