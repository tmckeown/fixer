package fixer.freeagent.com.fixer.model

data class Countries (
    val name: String,
    var isSelected: Boolean
)