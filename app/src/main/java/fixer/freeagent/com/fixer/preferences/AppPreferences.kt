package fixer.freeagent.com.fixer.preferences

import android.content.Context
import android.content.SharedPreferences
import fixer.freeagent.com.fixer.constraints.AppConstants

object AppPreferences {

    private lateinit var preferences: SharedPreferences
    private val COUNTRY_LIST = Pair(AppConstants.CONST_PREF_COUNTRY_LIST, "")


    /**
     * Init called by the  Fixer Application on app launch
     */
    fun init(context: Context) {
        preferences = context.getSharedPreferences(AppConstants.CONST_PREF_NAME, AppConstants.CONST_PREF_MODE_PRIVATE)
    }


    /**
     * Setup shared preference editor
     */
    private inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
        val editor = edit()
        operation(editor)
        editor.apply()
    }


    /**
     * Save a users selected country list in shared preferences
     */
    fun setCountryListToPrefs(countries: ArrayList<String>) {
        preferences.edit{
            it.putString(COUNTRY_LIST.first, convertArrayToString(countries))
        }
    }


    /**
     * Get country list as a list
     */
    fun getCountryListFromPrefs(): ArrayList<String> {
        val countryString: String? = preferences.getString(COUNTRY_LIST.first, AppConstants.CONST_API_COUNTRIES)
        val arrayListCountries: ArrayList<String> = ArrayList()
        if(countryString!!.contains(",")) {
            arrayListCountries.addAll(countryString.split(",") as ArrayList<String>)
        } else {
            arrayListCountries.add(0, countryString)
        }
        return arrayListCountries
    }


    /**
     * Get country list as comma seperated string specifically for web requests
     */
    fun getCountryListAsString(): String {
        val countries = getCountryListFromPrefs()
        return convertArrayToString(countries)
    }


    /**
     * Add a country to users country list if it does not already exist
     */
    fun addCountry(country: String) {
        val arrayListCountries = getCountryListFromPrefs()
        if(!arrayListCountries.contains(country)){
            arrayListCountries.add(country)
            setCountryListToPrefs(arrayListCountries)
        }
    }


    /**
     * Remove country from a users list if it exists
     */
    fun removeCountry(country: String) {
        val arrayListCountries = getCountryListFromPrefs()
        if(arrayListCountries.contains(country)) {
            arrayListCountries.remove(country)
            setCountryListToPrefs(arrayListCountries)
        }
    }


    /**
     * Convert list of countries to comma separated string representation
     */
    fun convertArrayToString(countries: ArrayList<String>) : String {
        val sb = StringBuilder()
        for (i in 0 until countries.size) {
            sb.append(countries[i])
            if(i+1 != countries.size) {
                sb.append(",")
            }
        }
        return sb.toString()
    }

}