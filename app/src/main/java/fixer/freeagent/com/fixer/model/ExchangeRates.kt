package fixer.freeagent.com.fixer.model

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type
import java.util.*

data class ExchangeRates(
    val base: String,
    val date: Date,
    val rates: RatesContainer,
    val success: Boolean,
    val timestamp: Int) {
}

data class RatesContainer (
    val ratesContainer: ArrayList<Rates>
)

data class Rates(
    val currencyName: String,
    val baseValue: Double,
    var displayValue: Double
)

/**
 * Custom GSON type adapter to parse rates from JsonObject and store them as list
 */
class RatesDeserializer : JsonDeserializer<RatesContainer> {
    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): RatesContainer  {
        val jsonObject = json.asJsonObject
        val listRates = ArrayList<Rates>()
        for (key in json.asJsonObject.keySet()) {
            val ratesLocal = Rates(key,
                jsonObject.get(key).asDouble,
                jsonObject.get(key).asDouble)
            listRates.add(ratesLocal)
        }
        return RatesContainer(listRates)
    }
}

