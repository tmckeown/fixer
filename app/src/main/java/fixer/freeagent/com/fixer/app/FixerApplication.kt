package fixer.freeagent.com.fixer.app

import android.app.Application
import fixer.freeagent.com.fixer.preferences.AppPreferences

class FixerApplication : Application() {

    /**
     * Init the shared preferences when application is launched
     */
    override fun onCreate() {
        super.onCreate()
        AppPreferences.init(this)
    }

}