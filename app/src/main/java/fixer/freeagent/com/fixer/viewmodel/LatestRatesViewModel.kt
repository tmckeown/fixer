package fixer.freeagent.com.fixer.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import fixer.freeagent.com.fixer.model.ExchangeRates
import fixer.freeagent.com.fixer.model.Rates
import fixer.freeagent.com.fixer.repository.LatestRatesRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class LatestRatesViewModel : ViewModel() {

    private val latestRatesRepository = LatestRatesRepository()
    val mutableListLatestRates = MutableLiveData<ArrayList<Rates>>()


    /**
     * API Call to get latest ratesContainer
     */
    fun callApiForLatestRates() {
        latestRatesRepository.requestLatestRatesForCurrencies()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(this::handleLatestRatesResponse, this::handleLatestRatesError)
    }


    /**
     * API Response for get latest ratesContainer
     */
    private fun handleLatestRatesResponse(exchangeRatesRemote: ExchangeRates) {
        mutableListLatestRates.postValue(exchangeRatesRemote.rates.ratesContainer)
    }


    /**
     * API Error response for get latest ratesContainer
     */
    private fun handleLatestRatesError(error: Throwable) {
        print(error.localizedMessage)
    }


    /**
     * Recalculate ratesContainer
     */
    fun recalculateLatestRates(amount: Double) {
        val ratesList = mutableListOf<Rates>();
        ratesList.addAll(mutableListLatestRates.value!!)
        ratesList.forEach {
            it.displayValue =  it.baseValue * amount
        }
        mutableListLatestRates.postValue(ratesList as ArrayList<Rates>?)
    }

}