package fixer.freeagent.com.fixer.repository

import com.google.gson.GsonBuilder
import fixer.freeagent.com.fixer.constraints.AppConstants
import fixer.freeagent.com.fixer.interfaces.FixerAPIService
import fixer.freeagent.com.fixer.model.ExchangeRates
import fixer.freeagent.com.fixer.model.RatesDeserializer
import fixer.freeagent.com.fixer.model.RatesContainer
import fixer.freeagent.com.fixer.preferences.AppPreferences
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class LatestRatesRepository {

    private val fixerApiService: FixerAPIService

    /**
     * init block setup retrofit and gson custom parsers
     */
    init {

        val gson = GsonBuilder()
            .registerTypeAdapter(RatesContainer::class.java, RatesDeserializer())
            .create()

        val retrofit = Retrofit.Builder()
            .baseUrl(AppConstants.CONST_API_BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
        fixerApiService = retrofit.create(FixerAPIService::class.java)
    }


    /**
     * Fixer API GET latest ratesContainer for currencies
     */
    fun requestLatestRatesForCurrencies(): Observable<ExchangeRates> {
        return fixerApiService.getLatestExchangeRatesForCurrencies(AppConstants.CONST_API_ACCESS_KEY, AppPreferences.getCountryListAsString())
    }

}