package fixer.freeagent.com.fixer.activities

import android.app.Dialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import fixer.freeagent.com.fixer.adapters.HistoricalRatesAdapter
import fixer.freeagent.com.fixer.model.ExchangeRates
import fixer.freeagent.com.fixer.viewmodel.HistoricalRatesViewModel
import kotlinx.android.synthetic.main.activity_historical_rates.*
import kotlinx.android.synthetic.main.include_toolbar.*
import android.widget.NumberPicker
import com.squareup.picasso.Picasso
import fixer.freeagent.com.fixer.R
import fixer.freeagent.com.fixer.constraints.AppConstants
import kotlinx.android.synthetic.main.dialog_country_picker.*

class ActivityHistoricalRates : AppCompatActivity() {

    // init view model
    private val historicalRatesViewModel: HistoricalRatesViewModel
        get() = ViewModelProviders.of(this).get(HistoricalRatesViewModel::class.java)

    // initial defaulted currencies
    private var amount: Double = 1.0
    private var leftCurrency: String = "USD"
    private var rightCurrency: String = "GBP"


    /**
     * Lifecycle method onCreate
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_historical_rates)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        getIntentExtra()
        setupObservables()
        attachOnClickListeners()
        setupInitialUIValues()
    }


    /**
     * Get parcel intent data
     */
    fun getIntentExtra() {
        amount = intent.getDoubleExtra(AppConstants.CONST_INTENT_AMOUNT, 1.0)
    }


    /**
     * Handle toolbar back button click
     */
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }


    /**
     * Setup observables to listen on view model
     */
    private fun setupObservables() {
        historicalRatesViewModel.mutableListLatestRates.observe(this, Observer {
            setupRecyclerView(it!!)
        })
        historicalRatesViewModel.amount = amount
        historicalRatesViewModel.callApiForHistoricalRates("EUR", "USD, GBP", amount)
    }


    /**
     * Setup Recycler View
     */
    private fun setupRecyclerView(exchangeRates: ArrayList<ExchangeRates>) {
        rv_historical_rates.layoutManager = LinearLayoutManager(this)
        rv_historical_rates.adapter = HistoricalRatesAdapter(exchangeRates, this)
    }


    /**
     * Attach OnClick Listeners
     */
    private fun attachOnClickListeners() {
        llLeftCountryPicker.setOnClickListener{
            show(true)
        }
        llRightCountryPicker.setOnClickListener{
            show(false)
        }
    }


    /**
     * Show a picker to allow a user to change currency
     */
    fun show(isLeftCountry: Boolean) {
        val d = Dialog(this, R.style.AppThemeDialogs)
        val countries = resources.getStringArray(R.array.str_array_countries)
        d.setContentView(R.layout.dialog_country_picker)
        val np = d.findViewById(R.id.countryPicker) as NumberPicker
        np.minValue = 0
        np.maxValue = countries.size-1
        np.displayedValues = countries
        np.wrapSelectorWheel = false
        d.button1.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                updateUIForNewCountryCode(isLeftCountry, countries[np.value])
                d.dismiss()
            }
        })
        d.show()
    }


    /**
     * Update the UI after a user has selected a different currency
     */
    fun updateUIForNewCountryCode(isLeftPicker: Boolean, country: String) {
        if(isLeftPicker) {
            Picasso.get().load(getImageUrl(country.substring(0,3))).into(ivLeftFlag)
            leftCurrency = country.take(3)
            tvLeftCurrencyName.setText(country.take(3))
        } else {
            Picasso.get().load(getImageUrl(country.take(3))).into(ivRightFlag)
            rightCurrency = country.take(3)
            tvRightCurrencyName.setText(country.take(3))
        }
        makeWebRequest()
    }


    /**
     * Render correct flags when activity starts currently defaulted to US and GB
     */
    private fun setupInitialUIValues() {
        Picasso.get().load(getImageUrl(leftCurrency)).into(ivLeftFlag)
        Picasso.get().load(getImageUrl(rightCurrency)).into(ivRightFlag)
    }


    /**
     * Get image URL by substituting the image name in the url
     */
    private fun getImageUrl(countryCode: String): String {
        return getString(R.string.str_flags_url, countryCode.substring(0,2))
    }


    /**
     * Call web service after currency has been switched by user
     */
    private fun makeWebRequest() {
        historicalRatesViewModel.callApiForHistoricalRates("EUR", leftCurrency+","+rightCurrency, amount)
    }

}
