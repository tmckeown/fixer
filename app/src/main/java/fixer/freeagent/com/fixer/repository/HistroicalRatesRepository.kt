package fixer.freeagent.com.fixer.repository

import com.google.gson.GsonBuilder
import fixer.freeagent.com.fixer.constraints.AppConstants
import fixer.freeagent.com.fixer.interfaces.FixerAPIService
import fixer.freeagent.com.fixer.model.ExchangeRates
import fixer.freeagent.com.fixer.model.RatesContainer
import fixer.freeagent.com.fixer.model.RatesDeserializer
import io.reactivex.Observable
import io.reactivex.functions.Function5
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class HistoricalRatesRepository {

    private val fixerApiService: FixerAPIService

    /**
     * init block setup retrofit and gson custom parser
     */
    init {

        val gsonTypeAdapter = GsonBuilder()
            .registerTypeAdapter(RatesContainer::class.java, RatesDeserializer())
            .create()

        val retrofit = Retrofit.Builder()
            .baseUrl(AppConstants.CONST_API_BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gsonTypeAdapter))
            .build()
        fixerApiService = retrofit.create(FixerAPIService::class.java)
    }


    /**
     * Fixer API GET historical ratesContainer for currencies
     *
     * Please note it would have been a better to have access to a paid account and request the time series data endpoint
     */
    fun requestHistoricalRates(dates: Array<String>, firstCurrency: String, secondCurrency: String): Observable<ArrayList<ExchangeRates>> {
        return Observable.zip(
            fixerApiService.getHistoricalRatesForCurrencies(dates[0], AppConstants.CONST_API_ACCESS_KEY, firstCurrency, secondCurrency),
            fixerApiService.getHistoricalRatesForCurrencies(dates[1], AppConstants.CONST_API_ACCESS_KEY, firstCurrency, secondCurrency),
            fixerApiService.getHistoricalRatesForCurrencies(dates[2], AppConstants.CONST_API_ACCESS_KEY, firstCurrency, secondCurrency),
            fixerApiService.getHistoricalRatesForCurrencies(dates[3], AppConstants.CONST_API_ACCESS_KEY, firstCurrency, secondCurrency),
            fixerApiService.getHistoricalRatesForCurrencies(dates[4], AppConstants.CONST_API_ACCESS_KEY, firstCurrency, secondCurrency),
            Function5<ExchangeRates, ExchangeRates, ExchangeRates, ExchangeRates, ExchangeRates, ArrayList<ExchangeRates>> {
                t1, t2, t3, t4, t5 ->
                arrayListOf(t1, t2, t3, t4, t5)
            })
    }

}