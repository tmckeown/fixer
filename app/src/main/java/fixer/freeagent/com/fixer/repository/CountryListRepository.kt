package fixer.freeagent.com.fixer.repository

import android.content.Context
import fixer.freeagent.com.fixer.R
import fixer.freeagent.com.fixer.model.Countries
import fixer.freeagent.com.fixer.preferences.AppPreferences

class CountryListRepository {

    /**
     * Fetch a list of countries from string resources update selected state based on preference data
     */
    fun fetchCountryList(context: Context) : ArrayList<Countries> {
        val countries = context.resources.getStringArray(R.array.str_array_countries)
        val userCountries = AppPreferences.getCountryListFromPrefs()
        val countryList: ArrayList<Countries> = ArrayList()
        countries.forEach {
            if(userCountries.contains(it.substring(0,3))) {
                countryList.add(Countries(it, true))
            } else {
                countryList.add(Countries(it, false))
            }
        }
        return countryList
    }

}