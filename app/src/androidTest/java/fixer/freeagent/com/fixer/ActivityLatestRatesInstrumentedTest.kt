package fixer.freeagent.com.fixer

import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.espresso.matcher.ViewMatchers.withText
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import fixer.freeagent.com.fixer.activities.ActivityLatestRates
import junit.framework.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ActivityLatestRatesInstrumentedTest {

    @get:Rule
    val activityTestRule = ActivityTestRule(ActivityLatestRates::class.java)

    @Test
    fun useAppContext() {
        val appContext = InstrumentationRegistry.getTargetContext()
        assertEquals("fixer.freeagent.com.fixer", appContext.packageName)
    }

    @Test
    fun testDefaultCurrency() {
        onView(withId(R.id.tvBaseCurrency)).check(matches(withText("EUR")))
    }

    @Test
    fun testInitialCurrencyAmount() {
        onView(withId(R.id.etAmount)).check(matches(withText("1")))
    }

}