package fixer.freeagent.com.fixer

import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import fixer.freeagent.com.fixer.activities.ActivityHistoricalRates
import junit.framework.TestCase.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ActivityHistoricalRatesInstrumentedTest {

    /**
     * See Activity Latest Rates Instrumentation Tests
     */
}