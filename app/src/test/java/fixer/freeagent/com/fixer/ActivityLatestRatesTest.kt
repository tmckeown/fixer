package fixer.freeagent.com.fixer

import android.support.v7.app.AlertDialog
import android.view.MenuItem
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import fixer.freeagent.com.fixer.activities.ActivityCountryList
import fixer.freeagent.com.fixer.activities.ActivityHistoricalRates
import fixer.freeagent.com.fixer.activities.ActivityLatestRates
import kotlinx.android.synthetic.main.activity_latest_rates.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.robolectric.Shadows
import org.robolectric.shadows.ShadowDialog

@RunWith(RobolectricTestRunner::class)
class ActivityLatestRatesTest {

    private var mActivity: ActivityLatestRates? = null

    /**
     * Initial setup of mActivity to test
     */
    @Before
    @Throws(Exception::class)
    fun init() {
        mActivity = Robolectric.buildActivity(ActivityLatestRates::class.java).create().get()
    }


    /**
     * Test the mActivity in not null
     */
    @Test
    @Throws(Exception::class)
    fun activityShouldNotBeNull() {
        assertNotNull(mActivity)
    }


    /**
     * Test the application has correct name
     */
    @Test
    @Throws(Exception::class)
    fun shouldHaveCorrectAppName() {
        val name = mActivity?.resources?.getString(R.string.app_name)
        assertThat(name, equalTo("Fixer"))
    }


    /**
     * Test country selection menu click results in correct mActivity launched
     */
    @Test
    @Throws(Exception::class)
    fun clickingToolbarMenuCountryList_shouldStartCountryListActivity() {
        // setup
        val menuItem = mock(MenuItem::class.java)
        val shadowActivity = Shadows.shadowOf(mActivity)

        shadowActivity.clickMenuItem(R.id.menuActionCountries);
        // intent
        val startedIntent = shadowActivity.nextStartedActivity
        val shadowIntent = Shadows.shadowOf(startedIntent)
        // assertion
        assertThat(shadowIntent.intentClass.canonicalName, equalTo(ActivityCountryList::class.java!!.canonicalName))
    }


    /**
     * Test historical rates menu click results in correct mActivity laumched
     */
    @Test
    @Throws(Exception::class)
    fun clickingToolbarMenuHistoricalRates_shouldStartHistoricalRates() {
        // setup
        val menuItem = mock(MenuItem::class.java)
        val shadowActivity = Shadows.shadowOf(mActivity)
        shadowActivity.clickMenuItem(R.id.menuActionHistorical);

        // intent
        val startedIntent = shadowActivity.nextStartedActivity
        val shadowIntent = Shadows.shadowOf(startedIntent)

        // assertion
        assertThat(shadowIntent.intentClass.canonicalName, equalTo(ActivityHistoricalRates::class.java!!.canonicalName))
    }


    /**
     * Test entering invalid amount show alert dialog
     */
    @Test
    @Throws(Exception::class)
    fun testEdtTxtAmount_HasCorrectStyle() {
        val editText = mActivity?.findViewById(R.id.etAmount) as EditText

        val textSize = (editText.textSize).toInt()
        assertEquals(20, textSize)

        val textValue = (editText.text.toString()).toInt()
        assertEquals(1, textValue)
    }


    /**
     * Test correct alert dialog is displayed for invalid amount entered
     */
    @Test
    @Throws(Exception::class)
    fun testEdtTxtValidation_shouldDisplayDialog() {
        mActivity!!.etAmount.setText("1001")
        mActivity!!.etAmount.onEditorAction(EditorInfo.IME_ACTION_DONE)
        val dialog = ShadowDialog.getLatestDialog() as AlertDialog
        assertThat("Validation Dialog Not Showing", dialog.isShowing)
        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).performClick();
        assertThat("Validation Dialog Did Not Dismiss", !dialog.isShowing)
    }

}