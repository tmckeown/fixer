# Fixer.io Android 

Android application written in Kotlin to consume Fixer.io API

## Getting Started

Checkout Master branch and run...

### Installing Build On Android Phone

Install via test flight by visiting the following URL on device https://tsfr.io/a53rd3

## Running the tests

Execute Roboletric tests from Android Studio

## Assumptions

When using fixer.io we should use the free tier and make best use of the free endpoints.

The brief specified to restrict currencies to 'USD, EUR, JPY, GBP, AUD, CAD, CHF, CNY, SEK, NZD.' as the parser allows it I consume the rest.

## With more time I would have

- Implemented local and remote data models including mappers to further decouple the web service.
- Moved all styling from layout files to the Styles.xml
- Better test coverage

## UI Issues

- It is not clear you can edit the amount in the latest rates screen.
- It is not clear you can change currency in historical rates by clicking the flag.
- Progress indicators would have been nice during API calls.
- Would have used a different navigation pattern.
- The country selector check box click area should be the complete cell.
- Should have the option to sort and search currencies.

## Screenshots

[Latest Rates](https://bitbucket.org/tmckeown/fixer/raw/master/readMeImages/img1.jpg)
[Country Selection](https://bitbucket.org/tmckeown/fixer/raw/master/readMeImages/img2.jpg)
[Historical Rates](https://bitbucket.org/tmckeown/fixer/raw/master/readMeImages/img3.jpg)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details


